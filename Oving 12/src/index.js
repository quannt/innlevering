// @flow
/* eslint eqeqeq: "off" */

import ReactDOM from "react-dom";
import * as React from "react";
import {Component} from "react-simplified";
import {HashRouter, Route, NavLink} from "react-router-dom";
import {Alert} from "./widgets";
import "bootstrap/dist/css/bootstrap.min.css";

import createHashHistory from "history/createHashHistory";

const history = createHashHistory(); // Use history.push(...) to programmatically change path


class Saker {
    id: number;
    static nextid = 1;
    tittel: string;
    dato: string;
    kategori: string;
    bilde: string;
    tekst: string;
    tid: string;

    constructor(
        tittel: string,
        dato: string,
        kategori: string,
        bilde: string,
        tekst: string,
        tid: string
    ) {

        this.tittel = tittel;
        this.dato = dato;
        this.kategori = kategori;
        this.bilde = bilde;
        this.tekst = tekst;
        this.tid = tid;
    }
}


class LiveFeed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("http://localhost:8080/livefeed", {
            method: "GET"
        })
            .then(res => res.json())
            .then(
                res => {
                    console.log(res);
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                error => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const {error, isLoaded, items} = this.state;
        var text = "";
        return (
            <div
                class="card"
                style={{width: "1200", margin: "auto", top: "10px"}}
            >
                <div class="card-body">
                    <marquee>
                        {items.map(Saker => (
                            <b style={{margin: "100px"}}>
                                {Saker.tittel} - {Saker.tid}
                            </b>
                        ))}
                    </marquee>
                </div>
            </div>
        );
    }
}

class Forside extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("http://localhost:8080/", {
            method: "GET"
        })
            .then(res => res.json())
            .then(
                res => {
                    console.log(res);
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                error => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }


    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div style={{background: "#040404"}}>
                    <div class="row mb-2" style={{margin: "20px"}}>
                        {items.map(Saker => (
                            <div class="col-md-6" key={Saker.id}>
                                <div
                                    class="card flex-md-row mb-4 shadow-sm h-md-250"
                                    style={{background: "#0B0505"}}
                                >
                                    <div class="card-body d-flex flex-column align-items-start">
                                        <strong class="d-inline-block mb-2 text-success">
                                            {Saker.kategori}
                                        </strong>
                                        <NavLink exact to={"/saker/" + Saker.id}>
                                            <h3 className="mb-0">
                                                <a className="text-dark" href="#">
                                                    {Saker.tittel}
                                                </a>
                                            </h3>
                                        </NavLink>
                                        <div class="mb-1 text-muted">{Saker.tid}</div>
                                        <NavLink exact to={"/saker/" + Saker.id}>
                                            Les Mer!
                                        </NavLink>

                                    </div>
                                    <img
                                        class="card-img-right flex-auto d-none d-lg-block"
                                        src={Saker.bilde}
                                        alt="Image"
                                        height="300px"
                                    />
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            );
        }
    }
}

class AlleSaker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("http://localhost:8080/saker", {
            method: "GET"
        })
            .then(res => res.json())
            .then(
                res => {
                    console.log(res);
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                error => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div style={{background: "#040404"}}>
                    <div class="row mb-2" style={{margin: "20px"}}>
                        {items.map(Saker => (
                            <div class="col-md-6" key={Saker.id}>
                                <div
                                    class="card flex-md-row mb-4 shadow-sm h-md-250"
                                    style={{background: "#0B0505"}}
                                >
                                    <div class="card-body d-flex flex-column align-items-start">
                                        <strong class="d-inline-block mb-2 text-success">
                                            {Saker.kategori}
                                        </strong>
                                        <NavLink exact to={"/saker/" + Saker.id}>
                                            <h3 className="mb-0">
                                                <a className="text-dark" href="#">
                                                    {Saker.tittel}
                                                </a>
                                            </h3>
                                        </NavLink>
                                        <div class="mb-1 text-muted">{Saker.tid}</div>
                                        <NavLink exact to={"/saker/" + Saker.id}>
                                            Les Mer!
                                        </NavLink>
                                    </div>
                                    <img
                                        class="card-img-right flex-auto d-none d-lg-block"
                                        src={Saker.bilde}
                                        alt="Image"
                                        height="300px"
                                    />
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            );
        }
    }
}

class Nyheter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("http://localhost:8080/nyheter", {
            method: "GET"
        })
            .then(res => res.json())
            .then(
                res => {
                    console.log(res);
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                error => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div style={{background: "#040404"}}>
                    <div class="row mb-2" style={{margin: "20px"}}>
                        {items.map(Saker => (
                            <div class="col-md-6" key={Saker.id}>
                                <div
                                    class="card flex-md-row mb-4 shadow-sm h-md-250"
                                    style={{background: "#0B0505"}}
                                >
                                    <div class="card-body d-flex flex-column align-items-start">
                                        <strong class="d-inline-block mb-2 text-success">
                                            {Saker.kategori}
                                        </strong>
                                        <NavLink exact to={"/nyheter/" + Saker.id}>
                                            <h3 className="mb-0">
                                                <a className="text-dark" href="#">
                                                    {Saker.tittel}
                                                </a>
                                            </h3>
                                        </NavLink>
                                        <div class="mb-1 text-muted">{Saker.tid}</div>
                                        <NavLink exact to={"/nyheter/" + Saker.id}>
                                            Les Mer!
                                        </NavLink>
                                    </div>
                                    <img
                                        class="card-img-right flex-auto d-none d-lg-block"
                                        src={Saker.bilde}
                                        alt="Image"
                                        height="300px"
                                    />
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            );
        }
    }
}

class Sport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("http://localhost:8080/sport", {
            method: "GET"
        })
            .then(res => res.json())
            .then(
                res => {
                    console.log(res);
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                error => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div style={{background: "#040404"}}>
                    <div class="row mb-2" style={{margin: "20px"}}>
                        {items.map(Saker => (
                            <div class="col-md-6" key={Saker.id}>
                                <div
                                    class="card flex-md-row mb-4 shadow-sm h-md-250"
                                    style={{background: "#0B0505"}}
                                >
                                    <div class="card-body d-flex flex-column align-items-start">
                                        <strong class="d-inline-block mb-2 text-success">
                                            {Saker.kategori}
                                        </strong>
                                        <NavLink exact to={"/sport/" + Saker.id}>
                                            <h3 className="mb-0">
                                                <a className="text-dark" href="#">
                                                    {Saker.tittel}
                                                </a>
                                            </h3>
                                        </NavLink>
                                        <div class="mb-1 text-muted">{Saker.tid}</div>
                                        <NavLink exact to={"/sport/" + Saker.id}>
                                            Les Mer!
                                        </NavLink>
                                    </div>
                                    <img
                                        class="card-img-right flex-auto d-none d-lg-block"
                                        src={Saker.bilde}
                                        alt="Image"
                                        height="300px"
                                    />
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            );
        }
    }
}

class Gaming extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("http://localhost:8080/gaming", {
            method: "GET"
        })
            .then(res => res.json())
            .then(
                res => {
                    console.log(res);
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                error => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div style={{background: "#040404"}}>
                    <div class="row mb-2" style={{margin: "20px"}}>
                        {items.map(Saker => (
                            <div class="col-md-6" key={Saker.id}>
                                <div
                                    class="card flex-md-row mb-4 shadow-sm h-md-250"
                                    style={{background: "#0B0505"}}
                                >
                                    <div class="card-body d-flex flex-column align-items-start">
                                        <strong class="d-inline-block mb-2 text-success">
                                            {Saker.kategori}
                                        </strong>
                                        <NavLink exact to={"/gaming/" + Saker.id}>
                                            <h3 className="mb-0">
                                                <a className="text-dark" href="#">
                                                    {Saker.tittel}
                                                </a>
                                            </h3>
                                        </NavLink>
                                        <div class="mb-1 text-muted">{Saker.tid}</div>
                                        <NavLink exact to={"/gaming/" + Saker.id}>
                                            Les Mer!
                                        </NavLink>
                                    </div>
                                    <img
                                        class="card-img-right flex-auto d-none d-lg-block"
                                        src={Saker.bilde}
                                        alt="Image"
                                        height="300px"
                                    />
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            );
        }
    }
}

class FilmogSerier extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("http://localhost:8080/filmogserier", {
            method: "GET"
        })
            .then(res => res.json())
            .then(
                res => {
                    console.log(res);
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                error => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div style={{background: "#040404"}}>
                    <div class="row mb-2" style={{margin: "20px"}}>
                        {items.map(Saker => (
                            <div class="col-md-6" key={Saker.id}>
                                <div
                                    class="card flex-md-row mb-4 shadow-sm h-md-250"
                                    style={{background: "#0B0505"}}
                                >
                                    <div class="card-body d-flex flex-column align-items-start">
                                        <strong class="d-inline-block mb-2 text-success">
                                            {Saker.kategori}
                                        </strong>
                                        <NavLink exact to={"/filmogserier/" + Saker.id}>
                                            <h3 className="mb-0">
                                                <a className="text-dark" href="#">
                                                    {Saker.tittel}
                                                </a>
                                            </h3>
                                        </NavLink>
                                        <div class="mb-1 text-muted">{Saker.tid}</div>
                                        <NavLink exact to={"/filmogserier/" + Saker.id}>
                                            Les Mer!
                                        </NavLink>
                                    </div>
                                    <img
                                        class="card-img-right flex-auto d-none d-lg-block"
                                        src={Saker.bilde}
                                        alt="Image"
                                        height="300px"
                                    />
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            );
        }
    }
}


class SlettogOppdater extends Component<{
    match: { params: { id: number } }
}> {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        console.log(this.props.match.params.id);
        fetch("http://localhost:8080/saker", {
            method: "GET"
        })
            .then(res => res.json())
            .then(
                res => {
                    console.log(res);
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                error => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div
                    class="mx-auto"
                    style={{ margin: "50px", height: "2000px", width: "1000px" }}
                >
                    <li
                        class="list-group list-group-item-action"
                        style={{width: "1000px"}}
                        key={Saker.id}
                    >
                        {items.map(Saker => (
                            <li
                                class="list-group-item list-group-item-action"
                                key={Saker.id}
                            >
                                {Saker.tittel} - {Saker.tid}
                                <NavLink
                                    style={{ margin: "10px", float: "right"}}
                                    exact
                                    to={"/oppdater/" + Saker.id}
                                    class="btn btn-primary"
                                >
                                    Oppdater
                                </NavLink>
                                <button
                                    type="delete"
                                    class="btn btn-danger"
                                    onClick={this.delete.bind(null, Saker.id)}
                                    style={{margin: "10px", float: "right"}}
                                >
                                    Slett
                                </button>
                            </li>
                        ))}
                    </li>
                </div>
            );
        }
    }

    delete(id: number) {
        const {error, isLoaded, items} = this.state;
        console.log(id);
        var url = "http://localhost:8080/saker/" + id;
        console.log(url);
        fetch(url, {
            method: "DELETE"
        }).then(res => res.json());
        this.setState({state: this.state});
    }


}

class OppdaterSak extends Component<{
    match: { params: { id: number } }
}> {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    tittel = "";
    tekst = "";
    bilde = "";

    componentDidMount() {
        var url = "http://localhost:8080/saker/" + this.props.match.params.id;
        fetch(url, {
            method: "GET"
        })
            .then(res => res.json())
            .then(
                res => {
                    console.log(res);
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                error => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const {error, isLoaded, items} = this.state;
        return (
            <div style={{margin: "20px", height: "8080px"}}>
                {items.map(Saker => (
                    <div>
                        <form>
                            <div class="form-group">
                                <label for="Tittel" style={{color: "white"}}>
                                    Tittel
                                </label>
                                <input
                                    type="title"
                                    defaultValue={Saker.tittel}
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.tittel = event.target.value)
                                    }
                                    class="form-control"
                                    id="Tittel"
                                />

                            </div>

                            <div class="form-group">
                                <label for="Innleggstekst" style={{color: "white"}}>
                                    Skriv din sak
                                </label>
                                <textarea
                                    type="text"
                                    defaultValue={Saker.tekst}
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.tekst = event.target.value)
                                    }
                                    class="form-control"
                                    id="Innleggstekst"
                                    rows="5"
                                />
                            </div>
                            <div class="form-group">
                                <label for="inpUrl" style={{color: "white"}}>
                                    Url til bilde
                                </label>
                                <input
                                    type="text"
                                    defaultValue={Saker.bilde}
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.bilde = event.target.value)
                                    }
                                    class="form-control"
                                    id="inpUrl"
                                />
                            </div>
                            <button type="submit" class="btn btn-primary" onClick={this.oppdater}>
                                Oppdater
                            </button>
                        </form>
                    </div>
                ))}
            </div>
        );
    }

    oppdater() {
        let url = "http://localhost:8080/saker/" + this.props.match.params.id;
        console.log(url);
        fetch(url, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({
                tittel: this.tittel,
                tekst: this.tekst,
                bilde: this.bilde,
                id: this.props.match.id
            })
        });
        history.push("/nysak");
    }
}

class NySak extends Component {
    tittel = "";
    kategori = "Nyheter";
    bilde = "";
    tekst = "";
    viktighet = "1";

    render() {
        return (
            <div class="mx-auto" style={{width: "1000px", margin: "20px"}}>
                <form>
                    <div class="form-group">
                        <label for="Tittel" style={{color: "white"}}>
                            Tittel
                        </label>
                        <input
                            type="title"
                            value={this.tittel}
                            onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                (this.tittel = event.target.value)
                            }
                            class="form-control"
                            id="Tittel"
                            placeholder="Tittel på innlegget"
                        />
                    </div>
                    <div class="form-group">
                        <label for="Kategori" style={{color: "white"}}>
                            Hvilket kategori tilhører din sak:
                        </label>
                        <select
                            class="form-control"
                            id="Kategori"
                            value={this.kategori}
                            onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                (this.kategori = event.target.value)
                            }
                        >
                            <option>Nyheter</option>
                            <option>Sport</option>
                            <option>Gaming</option>
                            <option>Film og Serier</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="Innleggstekst" style={{color: "white"}}>
                            Skriv din sak
                        </label>
                        <textarea
                            type="text"
                            value={this.tekst}
                            onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                (this.tekst = event.target.value)
                            }
                            class="form-control"
                            id="Innleggstekst"
                            rows="5"
                        />
                    </div>
                    <fieldset class="form-group">
                        <legend style={{color: "white"}}>Viktighet</legend>
                        <div class="form-check">
                            <label class="form-check-label" style={{color: "white"}}>
                                <input
                                    type="radio"
                                    class="form-check-input"
                                    name="optionsRadios"
                                    id="optionsRadios1"
                                    value="1"
                                    checked
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.viktighet = event.target.value)
                                    }
                                />
                                Denne saken vil komme på forsiden om du huker av for viktig
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label" style={{color: "white"}}>
                                <input
                                    type="radio"
                                    class="form-check-input"
                                    name="optionsRadios"
                                    id="optionsRadios2"
                                    value="0"
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.viktighet = event.target.value)
                                    }
                                />
                                Saken kommer kun under valgt kategori
                            </label>
                        </div>
                    </fieldset>
                    <div class="form-group">
                        <label for="inpUrl" style={{color: "white"}}>
                            Url til bilde
                        </label>
                        <input
                            type="text"
                            value={this.bilde}
                            onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                (this.bilde = event.target.value)
                            }
                            class="form-control"
                            id="inpUrl"
                            placeholder="NB! Linken kan ikke være lenger enn 200 tegn"
                        />
                    </div>
                    <button type="submit" class="btn btn-primary" onClick={this.ny}>
                        Send inn!
                    </button>
                </form>
            </div>
        );
    }

    ny() {
        console.log(this.tittel);
        console.log(this.viktighet);
        console.log(this.kategori);
        fetch("http://localhost:8080/saker", {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({
                tittel: this.tittel,
                tekst: this.tekst,
                bilde: this.bilde,
                kategori: this.kategori,
                viktighet: this.viktighet
            })
        });
        history.push("/");
    }

}

class Menu extends Component {
    render() {
        return (
            <nav
                class="navbar navbar-expand-lg navbar-dark bg-dark"
                style={{background: "#040404"}}
            >
                <NavLink class="navbar-brand" exact to="/">
                    <img
                        src="https://i1.sndcdn.com/artworks-000370673067-wuztt1-t500x500.jpg"
                        height="150"
                        width="150"
                        alt="Forside"
                    />
                </NavLink>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li className="nav-item">
                            <NavLink
                                class="nav-link"
                                exact
                                to="/saker"
                                style={{margin: "20px"}}
                            >
                                Alle Saker
                            </NavLink>
                        </li>
                        <li class="nav-item">
                            <NavLink
                                class="nav-link"
                                exact
                                to="/nyheter"
                                style={{margin: "20px"}}
                            >
                                Nyheter
                            </NavLink>
                        </li>
                        <li class="nav-item">
                            <NavLink
                                class="nav-link"
                                exact
                                to="/sport"
                                style={{margin: "20px"}}
                            >
                                Sport
                            </NavLink>
                        </li>
                        <li class="nav-item">
                            <NavLink
                                class="nav-link"
                                exact
                                to="/gaming"
                                style={{margin: "20px"}}
                            >
                                Gaming
                            </NavLink>
                        </li>
                        <li class="nav-item">
                            <NavLink
                                class="nav-link"
                                exact
                                to="/filmogserier"
                                style={{margin: "20px"}}
                            >
                                Film og Serier
                            </NavLink>
                        </li>

                    </ul>
                </div>
                <div
                    class="collapse navbar-collapse justify-content-end"
                    id="navbarNav"
                >
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <NavLink class="nav-link" exact to="/nysak">
                                Skriv ny sak
                            </NavLink>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}


class SjekkSak extends Component<{
    match: { params: { id: number } }
}> {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        var url = "http://localhost:8080/saker/" + this.props.match.params.id;
        console.log(url);
        fetch(url, {
            method: "GET"
        })
            .then(res => res.json())
            .then(
                res => {
                    console.log(res);
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                error => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div style={{margin: "1000px"}}>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div style={{margin: "1000px"}}>Loading...</div>;
        } else {
            return (
                <div
                    class="mx-auto"
                    style={{background: "#040404", height: "4000px", width: "1500px"}}
                >
                    <div class="col-md-8 blog-main" style={{margin: "20px"}}>
                        {items.map(Saker => (
                            <div class="blog-post">
                                <h2 class="blog-post-title" style={{color: "white"}}>
                                    {Saker.tittel}
                                </h2>
                                <p class="blog-post-meta" style={{color: "white"}}>
                                    {Saker.tid}
                                </p>

                                <img src={Saker.bilde} height="300px"/>
                                <hr style={{bordercolor: "white"}}/>
                                <p style={{color: "white"}}>{Saker.tekst}</p>
                            </div>
                        ))}
                    </div>
                </div>
            );
        }
    }
}


const root = document.getElementById("root");
if (root)
    ReactDOM.render(
        <HashRouter>
            <div style={{background: "#040404"}}>
                <Menu/>
                <LiveFeed/>
                <Route exact path="/" component={Forside}/>
                <Route path="/nysak" component={NySak}/>
                <Route path="/nysak" component={SlettogOppdater}/>
                <Route exact path="/saker" component={AlleSaker}/>
                <Route exact path="/nyheter" component={Nyheter}/>
                <Route exact path="/sport" component={Sport}/>
                <Route exact path="/gaming" component={Gaming}/>
                <Route exact path="/filmogserier" component={FilmogSerier}/>
                <Route exact path="/:id" component={SjekkSak}/>
                <Route exact path="/saker/:id" component={SjekkSak}/>
                <Route exact path="/nyheter/:id" component={SjekkSak}/>
                <Route exact path="/sport/:id" component={SjekkSak}/>
                <Route exact path="/gaming/:id" component={SjekkSak}/>
                <Route exact path="/filmogserier/:id" component={SjekkSak}/>
                <Route exact path="/oppdater/:id" component={OppdaterSak}/>
            </div>
        </HashRouter>,
        root
    );
