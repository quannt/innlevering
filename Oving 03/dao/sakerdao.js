// @flow

const Dao = require("./dao.js");

module.exports = class SakerDao extends Dao {
  //FORSIDE
  getAll(callback) {
    super.query(
      "SELECT id, tittel, tekst, bilde, DATE_FORMAT(dato, '%Y-%m-%d %H:%i') as 'tid', kategori, viktighet FROM saker WHERE viktighet=1 ORDER BY id DESC LIMIT 20",
      [],
      callback
    );
  }

  //Alle saker uansett viktighet
  getAll1(callback) {
    super.query(
      "SELECT id, tittel, tekst, bilde, DATE_FORMAT(dato, '%Y-%m-%d %H:%i') as 'tid', kategori, viktighet FROM saker ORDER BY id DESC LIMIT 20",
      [],
      callback
    );
  }

  getOne(id, callback) {
    super.query(
      "SELECT id, tittel, tekst, bilde, DATE_FORMAT(dato, '%Y-%m-%d %H:%i') as 'tid', kategori, viktighet FROM saker WHERE id=? LIMIT 20",
      [id],
      callback
    );
  }

  getNyhet(callback) {
    super.query(
      "SELECT id, tittel, tekst, bilde, DATE_FORMAT(dato, '%Y-%m-%d %H:%i') as 'tid', kategori, viktighet FROM saker WHERE kategori='Nyheter' LIMIT 20",
      [],
      callback
    );
  }

  getFilmogSerier(callback) {
    super.query(
      "SELECT id, tittel, tekst, bilde, DATE_FORMAT(dato, '%Y-%m-%d %H:%i') as 'tid', kategori, viktighet FROM saker WHERE kategori='Film og Serier' LIMIT 20",
      [],
      callback
    );
  }

  getSport(callback) {
    super.query(
      "SELECT id, tittel, tekst, bilde, DATE_FORMAT(dato, '%Y-%m-%d %H:%i') as 'tid', kategori, viktighet FROM saker WHERE kategori='Sport' LIMIT 20",
      [],
      callback
    );
  }

  getGaming(callback) {
    super.query(
      "SELECT id, tittel, tekst, bilde, DATE_FORMAT(dato, '%Y-%m-%d %H:%i') as 'tid', kategori, viktighet FROM saker WHERE kategori='Gaming' LIMIT 20",
      [],
      callback
    );
  }

  createOne(json, callback) {
    var val = [
      json.tittel,
      json.tekst,
      json.dato,
      json.bilde,
      json.kategori,
      json.viktighet
    ];
    super.query(
      "INSERT INTO saker (tittel, tekst, dato, bilde, kategori, viktighet) VALUES (?,?,?,?,?,?)",
      val,
      callback
    );
  }

  updateOne(json, id, callback) {
    var val = [json.tittel, json.tekst, json.bilde, id];
    super.query(
      "UPDATE saker SET tittel=?, tekst=?, bilde=? WHERE id=?",
      val,
      callback
    );
  }

  deleteOne(id, callback) {
    super.query("DELETE FROM saker WHERE id=?", [id], callback);
  }

  getSiste(callback) {
    super.query(
      "SELECT id, tittel, DATE_FORMAT(dato, '%H:%i') as 'tid' FROM saker ORDER BY id DESC LIMIT 10;",
      [],
      callback
    );
  }
};
