SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS saker;
DROP TABLE IF EXISTS kategorier;
SET FOREIGN_KEY_CHECKS=1;

CREATE TABLE kategorier (
kategori VARCHAR (20) NOT NULL,
PRIMARY KEY (kategori)
);

CREATE TABLE saker (
id INT NOT NULL AUTO_INCREMENT,
tittel VARCHAR (256) NOT NULL,
tekst TEXT NOT NULL,
dato TIMESTAMP NOT NULL,
bilde VARCHAR(250),
kategori VARCHAR (20) NOT NULL,
viktighet TINYINT,
PRIMARY KEY (id),
FOREIGN KEY (kategori) REFERENCES kategorier(kategori)
);

Insert into kategorier (kategori) VALUES ('Nyheter'), ('Sport'), ('Gaming'), ('Film og Serier');

INSERT INTO saker (tittel, tekst, bilde, kategori, viktighet) VALUES ('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Film og Serier', 0),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Nyheter', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Sport', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1),
('Hei på deg', 'Dette er en test', 'https://i.ytimg.com/vi/kS98tDJm7Pw/maxresdefault.jpg', 'Gaming', 1);
