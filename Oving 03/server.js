var express = require("express");
var mysql = require("mysql");
var bodyParser = require("body-parser");
var app = express();
var apiRoutes = express.Router();
app.use(bodyParser.json()); // for å tolke JSON
const SakerDao = require("./dao/sakerdao.js");
var cors = require("cors");
app.use(cors());

var pool = mysql.createPool({
  connectionLimit: 2,
  host: "mysql.stud.iie.ntnu.no",
  user: "quannt",
  password: "UX8fRcx1",
  database: "quannt",
  debug: false
});

let sakerDao = new SakerDao(pool);

//Få alle saker med viktighet 1 (forside)
app.get("/", (req, res) => {
  console.log("/forside: fikk request fra klient!");
  sakerDao.getAll((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Få alle nyhetssaker uansett viktighet
app.get("/saker", (req, res) => {
  console.log("/saker: fikk request fra klient!");
  sakerDao.getAll1((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Få spesifikk sak
app.get("/saker/:id", (req, res) => {
  console.log("/saker/:id: fikk request fra klient!");
  sakerDao.getOne(req.params.id, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Få alle nyhetssaker
app.get("/nyheter", (req, res) => {
  console.log("/nyheter: fikk request fra klient!");
  sakerDao.getNyhet((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Få sport
app.get("/sport", (req, res) => {
  console.log("/sport: fikk request fra klient!");
  sakerDao.getSport((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Gaming
app.get("/gaming", (req, res) => {
  console.log("/gaming: fikk request fra klient!");
  sakerDao.getGaming((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Film og serier
app.get("/filmogserier", (req, res) => {
  console.log("/filmogserier: fikk request fra klient!");
  sakerDao.getFilmogSerier((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Ny sak post
app.post("/saker", (req, res) => {
  console.log("Fikk POST-request fra klient!");
  sakerDao.createOne(req.body, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Slett en sak
app.delete("/saker/:id", (req, res) => {
  console.log("/saker/:id: fikk DELETE request fra klient!");
  sakerDao.deleteOne(req.params.id, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Få de 10 siste (live feed)
app.get("/livefeed", (req, res) => {
  console.log("/livefeed: GET request om siste saker!");
  sakerDao.getSiste((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Oppdatere
app.put("/saker/:id", (req, res) => {
  console.log("/saker/:id: fikk PUT request fra klient!");
  sakerDao.updateOne(req.body, req.params.id, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

var server = app.listen(8080);
